<?php
namespace App\MyLib;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class JWTHelper
{
    public static function tokenIsValid($token) {
        $key = env('JWT_KEY');
        $pieces = explode(".", $token);
        $signature = hash_hmac('SHA256', "$pieces[0].$pieces[1]", $key, true);
        $pieces[2] = base64_decode($pieces[2]);
        if ($pieces[2] == $signature) {
            $profil = getProfil($token);
            if (loginIsValid($profil->login)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static function getProfil ($token) {
        $pieces = explode(".", $token);
        $pieces[1] = json_decode(base64_decode($pieces[1]));
        return $pieces[1];
    }
    
    public static function createToken(AuthenticatableContract $utilisateur, $moreInfos)
    {
        $key = env('JWT_KEY');
        $includes = explode(',', env('JWT_INCLUDES'));
        $valid_time = env('JWT_VALID_TIME');
        $issuer = env('JWT_ISSUER');
        
        //build the headers
        $headers = ['alg' => 'HS256', 'typ' => 'JWT'];
        $headers_encoded = base64_encode(json_encode($headers));
        
        //build the payload
        $issued_at = time();
        $expire = $issued_at + $valid_time;
        $payload = [
        "iss" => $issuer,
        "iat" => $issued_at,
        "exp" => $expire,
        ];
        foreach ($includes as $fieldToInclude) {
            $payload[$fieldToInclude] = $utilisateur->{$fieldToInclude};
        }
        if (isset($moreInfos)) { // inclure les infos recus en paramètres
            foreach ($moreInfos as $key => $info) {
                $payload[$key] = $info;
            }
        }
        
        $payload_encoded = base64_encode(json_encode($payload));
        
        //build the signature
        $signature = hash_hmac('SHA256', "$headers_encoded.$payload_encoded", $key, true);
        $signature_encoded = base64_encode($signature);
        
        //build and return the token
        $token = "$headers_encoded.$payload_encoded.$signature_encoded";
        return $token;
    }
}