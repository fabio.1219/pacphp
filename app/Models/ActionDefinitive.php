<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionDefinitive extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pac_actiondefinitive';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'acd_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
