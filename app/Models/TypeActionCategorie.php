<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeActionCategorie extends Model
{
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'typeactionCategorie';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = true;
    
    /**
    * The attributes that should be visible in arrays.
    *
    * @var array
    */
    protected $visible = ['categorie'];
    
    protected $fillable = ['categorie'];
}