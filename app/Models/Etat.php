<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etat extends Model
{
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'etat';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;
    
    public function etataction()
    {
        return $this->hasMany('App\EtatAction', 'etat');
    }
}