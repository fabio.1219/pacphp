<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistiques extends Model
{
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'statistiques';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = true;
    
    protected $casts = [
    'groupes' => 'array'
    ];
}