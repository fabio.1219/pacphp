<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'client';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = true;
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [];
    
    protected $dates = ['deleted_at'];
    
    public function action()
    {
        return $this->hasMany('App\Action', 'client');
    }
    
    public function referant()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'referant');
    }
    
    public function categorie()
    {
        return $this->belongsTo('App\Models\Categorie', 'categorie');
    }
    
    public function comptesrendus()
    {
        return $this->hasMany('App\Models\Compterendu', 'client');
    }
}