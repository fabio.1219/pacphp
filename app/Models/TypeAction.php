<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeAction extends Model
{
    use SoftDeletes;
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'typeaction';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;
    
    protected $dates = ['deleted_at'];
    
    protected $hidden = ['typeactioncategorie']; // hide teams relation in JSON output
    
    protected $appends = ['categories']; // add custom attribute to JSON output
    
    
    public function getCategoriesAttribute(){ // applatir les objets typeactioncategorie en catégorie
        return $this->typeactioncategorie->sortBy('categorie')->pluck('categorie');
    }
    
    public function typeactioncategorie()
    {
        return $this->hasMany('App\Models\TypeActionCategorie', 'typeaction');
    }
    
}