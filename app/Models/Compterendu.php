<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compterendu extends Model
{
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'compterendu';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = true;
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [];
    
    public function utilisateur()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'utilisateur');
    }
}