<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'action';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = true;
    
    protected $appends = ['etatActuel']; // add custom attribute to JSON output
    
    public function getEtatActuelAttribute(){
        return $this->etataction->last();
    }
    
    public function etataction()
    {
        return $this->hasMany('App\Models\EtatAction', 'action');
    }
    
    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client');
    }
    
    public function typeaction()
    {
        return $this->belongsTo('App\Models\TypeAction', 'typeaction');
    }
}