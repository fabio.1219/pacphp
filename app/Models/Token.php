<?php

namespace App\Models;

use Log;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'token';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;
    
    protected $hidden = ['id', 'utilisateur', 'token']; // hide teams relation in JSON output
    
    public function utilisateurToken()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'utilisateur');
    }
}