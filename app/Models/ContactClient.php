<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactClient extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contactclient';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
