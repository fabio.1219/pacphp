<?php

namespace App\Models;

use Log;
use Illuminate\Database\Eloquent\Model;

class EtatAction extends Model
{
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'etataction';
    
    /**
    * Primary key
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = true;
    
    protected $hidden = ['action']; // hide teams relation in JSON output
    
    public function etat()
    {
        return $this->belongsTo('App\Models\Etat', 'etat');
    }
    
    public function utilisateur()
    {
        return $this->belongsTo('App\Models\Utilisateur', 'utilisateur');
    }
}