<?php
namespace App\Http\Middleware;

use Closure;
use App\Models\Utilisateur;
use App\Models\Token;
use App\MyLib\JWTHelper;
use \Illuminate\Http\Request;
use \Illuminate\Http\Response;

class AuthMiddleware
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->header('Authorization')) {
            //return response('Unauthorized.', 401);
            return $next($request); // pour pouvoir essayer dans le navigateur, comme ca en mode dev je peux quand meme essayer
        }
        
        $parttoken = explode(' ', $request->header('Authorization'));
        $token = $parttoken[1];
        $tokenFind = Token::where('token', $token)->first();
        if ($tokenFind) {
            $utilisateur = $tokenFind->utilisateurToken;
            $request->request->add(['utilisateur' => $utilisateur, 'token' => $token]);
            return $next($request);
        }
        else {
            return response('Unauthorized.', 401);
        }
    }
}