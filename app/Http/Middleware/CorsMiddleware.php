<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use \Illuminate\Http\Request;
use \Illuminate\Http\Response;

class CorsMiddleware
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle(Request $request, Closure $next)
    {
        //if ($request->header('Authorization') == "salut") {
        //   return $next($request);
        //}
        
        /*if ($request->header('Authorization') == "salut") {
        return (new Response(null, 200));
        }*/
        
        if ($request->isMethod('OPTIONS'))
        {
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent('OK');
            return $response
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
        }
        
        return $next($request)
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        //return $next($request);
    }
}