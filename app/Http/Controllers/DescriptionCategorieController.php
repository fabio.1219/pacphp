<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use Illuminate\Http\Response;

class DescriptionCategorieController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }
    
    public function getCategories()
    {
        $categories = Categorie::all();
        return response()->json($categories);
    }
    
    public function getCategorie($id)
    {
        $categorie = Categorie::find($id);
        if (!$categorie) {
            return (new Response(null, 404));
        }
        return response()->json($categorie);
    }
}