<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Client;
use App\Models\Categorie;
use App\Models\Statistiques;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StatistiqueController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct() {}
    
    public function getStatistiquesGroupes()
    {
        return Client::where('categorie', '>', 0)->pluck('groupe')->unique()->map(function ($groupe) {
            $clients = Client::where('groupe', $groupe)->where('categorie', '>', 0);
            $nbClients = $clients->count();
            
            $init = (object) array(
            'connaissance' => 0,
            'potentiel' => 0,
            'sansCategorie' => 0
            );
            
            $valeurConnaissance = function ($connaissance) {
                if ($connaissance == 'basse') {
                    return 1;
                }
                if ($connaissance == 'moyenne') {
                    return 2;
                }
                if ($connaissance == 'élevée') {
                    return 3;
                }
            };
            
            $valeurPotentiel = function ($potentiel) {
                if ($potentiel == 'faible') {
                    return 1;
                }
                if ($potentiel == 'moyen') {
                    return 2;
                }
                if ($potentiel == 'fort') {
                    return 3;
                }
            };
            
            $total = $clients->get()->reduce(function ($accumulator, $client) use ($valeurConnaissance, $valeurPotentiel) {
                $categorie = Categorie::find($client->categorie);
                $accumulator->connaissance += $valeurConnaissance($categorie->connaissance);
                $accumulator->potentiel += $valeurPotentiel($categorie->potentiel);
                return $accumulator;
            }, $init);
            
            return (object) array(
            'nom' => $groupe,
            'connaissance' => ($total->connaissance / $nbClients),
            'potentiel' => ($total->potentiel / $nbClients),
            'total' => $nbClients
            );
        })->values();
    }
    
    public function getStatistiquesClients (Request $request)
    {
        $nbClients = Client::count();
        $sansCategories = Client::where('categorie', '=', 0)->count();
        $groupes = $this->getStatistiquesGroupes();
        return response()->json(
        (object) array(
        'nbClients' => $nbClients,
        'sansCategories' => $sansCategories,
        'groupes' => $groupes
        )
        );
    }
    
    public function getStatistiquesEnregistrees (Request $request)
    {
        $statistiques = Statistiques::orderBy('id', 'desc')->paginate(15);
        return response()->json($statistiques);
    }
    
    public function postStatistiquesEnregistrees (Request $request)
    {
        $data = json_decode($request->getContent());
        $statistiques = new Statistiques();
        $statistiques->nbClients = $data->nbClients;
        $statistiques->sansCategories = $data->sansCategories;
        $statistiques->groupes = $data->groupes;
        $statistiques->save();
        return response()->json(['status' => 'success', 'id' => $statistiques->id]);
    }
    
    public function delStatistiquesEnregistrees($id)
    {
        $statistiques = Statistiques::find($id);
        $statistiques->delete();
        return response()->json(['status' => 'success']);
    }
}