<?php

namespace App\Http\Controllers;

use App\Models\Etat;
use Illuminate\Http\Response;

class EtatController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct() {}
    
    public function getEtats()
    {
        $etats = Etat::all();
        return response()->json($etats);
    }
}