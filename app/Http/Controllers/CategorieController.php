<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Response;

class CategorieController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }
    
    public function getCategorie($idCategorie, $idReferant)
    {
        $clients;
        if ($idCategorie > -1) {
            $clients = Client::where('categorie', '=', $idCategorie);
            if ($idReferant > -1) { $clients->where('referant', '=', $idReferant); }
        } else if ($idReferant > -1) {
            $clients = Client::where('referant', '=', $idReferant);
        }
        
        $clients = $clients->with(['referant', 'comptesrendus', 'comptesrendus.utilisateur'])->paginate(15);
        
        return response()->json($clients);
    }
}