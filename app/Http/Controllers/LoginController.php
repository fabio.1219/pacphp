<?php
namespace App\Http\Controllers;

use Log;
use App\Models\Utilisateur;
use App\Models\Token;
use App\MyLib\JWTHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
    }
    
    private function addToken ($idUtilisateur, $token)
    {
        $newToken = new Token();
        $newToken->utilisateur = $idUtilisateur;
        $newToken->token = $token;
        $newToken->save();
    }
    
    private function removeToken ($idUtilisateur, $token)
    {
        $matchThese = ['utilisateur' => $idUtilisateur, 'token' => $token];
        Token::where($matchThese)->delete();
    }
    
    
    public function login(Request $request)
    {
        $data = json_decode($request->getContent());
        $utilisateur = Utilisateur::where('login', $data->login)->first();
        if ($utilisateur) {
            if (Hash::check($data->password, $utilisateur->password)) {
                $moreInfos = [];
                if (Hash::check("pac123", $utilisateur->password)) { //mdp par défaut
                    $moreInfos['changemdp'] = true;
                }
                $token = JWTHelper::createToken($utilisateur, $moreInfos);
                $this->addToken($utilisateur->id, $token);
                $response = ['status' => 'success', 'token' => $token];
                
                return response()->json($response);
            }
            else {
                return response()->json(['status' => 'fail']);
            }
        }
        else {
            return response()->json(['status' => 'fail']);
        }
    }
    
    public function changerMdp(Request $request)
    {
        $data = json_decode($request->getContent());
        
        $utilisateur = Utilisateur::find($request->utilisateur->id);
        $utilisateur->password = app('hash')->make($data->password);
        $utilisateur->save();
        
        $token = JWTHelper::createToken($utilisateur, []);
        $this->removeToken($utilisateur->id, $request->token);
        $this->addToken($utilisateur->id, $token);
        
        $response = ['status' => 'success', 'token' => $token];
        
        return response()->json($response);
    }
    
    public function logout(Request $request)
    {
        $this->removeToken($request->utilisateur->id, $request->token);
        
        $response = ['status' => 'success'];
        
        return response()->json($response);
    }
}