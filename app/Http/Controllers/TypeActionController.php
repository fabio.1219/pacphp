<?php

namespace App\Http\Controllers;

use Log;
use App\Models\TypeAction;
use App\Models\TypeActionCategorie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TypeActionController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }
    
    public function getTypesAction(Request $request)
    {
        if($request->input('paginate')) {
            $typesAction = TypeAction::paginate(15);
        } else {
            $typesAction = TypeAction::all();
        }
        return response()->json($typesAction);
    }
    
    public function getTypeAction($id)
    {
        $typeAction = TypeAction::find($id);
        if (!$typeAction) {
            return (new Response(null, 404));
        }
        return response()->json($typeAction);
    }
    
    public function postTypeAction(Request $request)
    {
        $data = json_decode($request->getContent());
        $typeaction = new TypeAction();
        $typeaction->nom = ucfirst($data->nom);
        
        //transformer l'array tout simple de no de categorie pour l'insertion
        $categories = array_map(
        function($categorie) { return ['categorie' => $categorie]; }, $data->categories
        );
        
        $typeaction->save();
        
        //insertion des categories
        $typeaction->typeactioncategorie()->createMany($categories);
        
        /* recupere le tout */
        //return response()->json(['status' => 'success', 'data' => $typeaction]);
        return response()->json(['status' => 'success', 'id' => $typeaction->id]);
    }
    
    
    public function putTypeAction($id, Request $request)
    {
        $data = json_decode($request->getContent());
        $typeaction = TypeAction::find($id);
        
        $data->nom = ucfirst($data->nom);
        if ($typeaction->nom !== $data->nom) {
            $typeaction->nom = $data->nom;
        }
        
        $typeaction->typeactioncategorie()->delete();
        
        //transformer l'array tout simple de no de categorie pour l'insertion
        $categories = array_map(
        function($categorie) { return ['categorie' => $categorie]; }, $data->categories
        );
        
        //insertion des categories
        $typeaction->typeactioncategorie()->createMany($categories);
        
        $typeaction->save();
        
        /* recupere le tout */
        $typeaction = TypeAction::find($id);
        return response()->json(['status' => 'success', 'data' => $typeaction]);
    }
    
    public function delTypeAction($id)
    {
        $typeaction = TypeAction::find($id);
        $typeaction->delete();
        return response()->json(['status' => 'success']);
    }
}