<?php

namespace App\Http\Controllers;

use App\Models\ContactClient;
use Illuminate\Http\Response;

class ContactClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}
    
    public function getContactsClients()
    {
        $contactsClients = ContactClient::all();
        return response()->json($contactsClients);
    }
    
    public function getcontactClient($id)
    {
        $contactClient = ContactClient::find($id);
        if (!$contactClient) {
            return (new Response(null, 404));
        }
        return response()->json($contactClient);
    }
}
