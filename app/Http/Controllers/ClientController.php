<?php

namespace App\Http\Controllers;

use Log;
use App\Models\ContactClient;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClientController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct() {}
    
    public function getClients(Request $request)
    {
        if($request->input('nom')) { return $this->getClientsByName($request->input('nom')); }
        $clients = Client::with(['referant', 'comptesrendus', 'comptesrendus.utilisateur'])->paginate(15);
        return response()
        ->json($clients);
    }
    
    public function getClientsByName($name)
    {
        $clients = Client::where('nom', 'like', "%${name}%")->with(['referant', 'comptesrendus', 'comptesrendus.utilisateur'])->paginate(15);
        return response()
        ->json($clients);
    }
    
    public function getClient($id)
    {
        $client = Client::with(['referant', 'comptesrendus', 'comptesrendus.utilisateur'])->find($id);
        $client->contacts = ContactClient::where('client', $id)->get();
        if (!$client) {
            return (new Response(null, 404));
        }
        return response()->json($client);
    }
    
    public function postClient(Request $request)
    {
        $data = json_decode($request->getContent());
        $client = new Client();
        $client->nom = $data->nom;
        $client->groupe = $data->groupe;
        $client->categorie = $data->categorie;
        $client->secteur = $data->secteur;
        $client->referant = $data->referant->id;
        $client->save();
        foreach ($data->contacts as $dataContact){
            $contact = new ContactClient();
            $contact->nom = $dataContact->nom;
            if ($dataContact->telephone) { $contact->telephone = $dataContact->telephone; }
            if ($dataContact->email) { $contact->email = $dataContact->email; }
            $contact->client = $client->id;
            $contact->save();
        }
        return response()->json(['status' => 'success', 'id' => $client->id]);
    }
    
    public function putClient($id, Request $request)
    {
        $data = json_decode($request->getContent());
        $client = Client::find($id);
        if ($client->nom !== $data->nom) { $client->nom = $data->nom; }
        if ($client->groupe !== $data->groupe) { $client->groupe = $data->groupe; }
        if ($client->categorie !== $data->categorie) { $client->categorie = $data->categorie; }
        if ($client->secteur !== $data->secteur) { $client->secteur = $data->secteur; }
        if ($client->referant !== $data->referant->id) { $client->referant = $data->referant->id; }
        $client->save();
        foreach ($data->contacts as $dataContact) {
            if (isset($dataContact->deleted) && $dataContact->deleted) {
                if (isset($dataContact->id)) { // s'il faut supprimer, et qu'il existait (= qu'il a un id)
                    $contact = ContactClient::find($dataContact->id);
                    $contact->delete();
                }
            } else {
                if (isset($dataContact->id)) {
                    $contact = ContactClient::find($dataContact->id);
                } else {
                    $contact = new ContactClient();
                }
                if ($contact->nom !== $dataContact->nom) { $contact->nom = $dataContact->nom; }
                if ($contact->telephone !== $dataContact->telephone) { $contact->telephone = $dataContact->telephone; }
                if ($contact->email !== $dataContact->email) { $contact->email = $dataContact->email; }
                if ($contact->client !== $id) { $contact->client = $id; }
                $contact->save();
            }
        }
        /* recupere le tout */
        $client = Client::with(['referant', 'comptesrendus', 'comptesrendus.utilisateur'])->find($id);
        $client->contacts = ContactClient::where('client', $id)->get();
        return response()->json(['status' => 'success', 'data' => $client]);
    }
    
    public function delClient($id)
    {
        $client = Client::find($id);
        $client->delete();
        return response()->json(['status' => 'success']);
    }
}