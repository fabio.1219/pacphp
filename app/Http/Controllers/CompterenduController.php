<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Compterendu;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CompterenduController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }
    
    public function postCompterendu(Request $request)
    {
        $data = json_decode($request->getContent());
        $compterendu = new Compterendu();
        $compterendu->texte = $data->texte;
        $compterendu->client = $data->client;
        $compterendu->utilisateur = $request->utilisateur->id;
        
        $compterendu->save();
        
        return response()->json(['status' => 'success']);
    }
}