<?php

namespace App\Http\Controllers;

use App\Models\ActionDefinitive;
use Illuminate\Http\Response;

class ActionDefinitiveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function getActionsDefinitives()
    {
        $actionsdefinitives = ActionDefinitive::all();
        return response()->json($actionsdefinitives);
    }
    
    public function getActionDefinitive($id)
    {
        $actiondefinitive = ActionDefinitive::find($id);
        if (!$actiondefinitive) {
            return (new Response(null, 404));
        }
        return response()->json($actiondefinitive);
    }
}
