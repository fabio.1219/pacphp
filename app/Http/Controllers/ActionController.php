<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Action;
use App\Models\EtatAction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ActionController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }
    
    public function getActions(Request $request)
    {
        if($request->input('nom') || $request->input('etat') || $request->input('dateEcheance')) {
            return $this->getActionsFiltered($request);
        }
        $actions = Action::with(['client', 'client.referant', 'typeaction', 'etataction.etat', 'etataction.utilisateur'])->paginate(15);
        return response()->json($actions);
    }
    
    public function getActionsFiltered(Request $request)
    {
        $actions = Action::with('client', 'client.referant', 'typeaction', 'etataction.etat', 'etataction.utilisateur');
        if ($request->input('nom')) {
            $name = $request->input('nom');
            $actions = $actions->whereHas('client', function ($query) use ($name) {
                $query->where('nom', 'like', "%$name%");
            });
        }
        if ($request->input('etat')) {
            $etat = $request->input('etat');
            $actions = $actions->whereHas('etataction', function ($query) use ($etat) {
                $query->where('etat', $etat);
            });
        }
        $dateEcheance = json_decode($request->input('dateEcheance'));
        if (isset($dateEcheance->formatDate)) {
            list($day, $month, $year) = explode('.', $dateEcheance->formatDate);
            $dateComparaison = "$year.$month.$day";
            $actions = $actions->where('dateEcheanceCompare', '<=', $dateComparaison);
        }
        $datePrevue = json_decode($request->input('datePrevue'));
        if (isset($datePrevue->formatDate)) {
            list($day, $month, $year) = explode('.', $datePrevue->formatDate);
            $dateComparaison = "$year.$month.$day";
            $actions = $actions->whereNotNull('datePrevueCompare')->where('datePrevueCompare', '<=', $dateComparaison);
        }
        if ($request->input('datePassee') === 'true') {
            $today = date("Y.m.d");
            $actions = $actions
            ->whereNull('datePrevueCompare')
            ->where('dateEcheanceCompare', '<', $today);
        }
        $actions = $actions->paginate(15);
        return response()->json($actions);
    }
    
    public function getAction($id)
    {
        $action = Action::with('client', 'client.referant', 'typeaction', 'etataction.etat', 'etataction.utilisateur')->find($id);
        return response()->json($action);
    }
    
    public function postAction(Request $request)
    {
        $data = json_decode($request->getContent());
        $action = new Action();
        $action->client = $data->client;
        $action->typeaction = $data->typeaction;
        $action->dateEcheance = $data->dateEcheance;
        list($day, $month, $year) = explode('.', $data->dateEcheance);
        $action->dateEcheanceCompare = "$year.$month.$day";
        
        $action->save();
        
        $etatAction = new EtatAction();
        $etatAction->action = $action->id;
        $etatAction->utilisateur = $request->utilisateur->id;
        if ($data->horsCategorie == false) {
            $etatAction->etat = 2; // Etat "Envisagée"
        } else {
            $etatAction->etat = 1; // Etat "A valider"
            $etatAction->commentaire = $data->commentaire;
        }
        $etatAction->save();
        
        $action = Action::with(['client', 'client.referant', 'typeaction', 'etataction.etat', 'etataction.utilisateur'])->find($action->id);
        
        return response()->json(['status' => 'success', 'data' => $action]);
    }
    
    public function putAction($id, Request $request)
    {
        //mise à jour de l'action
        $data = json_decode($request->getContent());
        $action = Action::find($id);
        
        if (isset($data->action)) {
            $dataAction = $data->action;
            if (isset($dataAction->dateEcheance) && $action->dateEcheance !== $dataAction->dateEcheance) {
                $action->dateEcheance = $dataAction->dateEcheance;
                list($day, $month, $year) = explode('.', $dataAction->dateEcheance);
                $action->dateEcheanceCompare = "$year.$month.$day";
            }
            if (isset($dataAction->datePrevue) && $action->datePrevue !== $dataAction->datePrevue) {
                $action->datePrevue = $dataAction->datePrevue;
                list($day, $month, $year) = explode('.', $dataAction->datePrevue);
                $action->datePrevueCompare = "$year.$month.$day";
            }
            if (isset($dataAction->detail) && $action->detail !== $dataAction->detail) {
                $action->detail = $dataAction->detail;
            }
            $action->save();
        }
        
        //nouvelle état
        if (isset($data->etat)) {
            $dataEtat = $data->etat;
            $etatAction = new EtatAction();
            $etatAction->action = $action->id;
            $etatAction->utilisateur = $request->utilisateur->id;
            $etatAction->etat = $dataEtat->etat;
            if (isset($dataEtat->commentaire)) {
                $etatAction->commentaire = $dataEtat->commentaire;
            }
            $etatAction->save();
        }
        
        /* recupere le tout */
        $action = Action::with(['client', 'client.referant', 'typeaction', 'etataction.etat', 'etataction.utilisateur'])->find($id);
        return response()->json(['status' => 'success', 'data' => $action]);
    }
}