<?php
namespace App\Http\Controllers;

use App\Models\Utilisateur;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UtilisateurController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
    }
    
    public function getUtilisateurs(Request $request)
    {
        if ($request->input('nom')) {
            return $this->getUtilisateursByName($request->input('nom'));
        }
        $utilisateurs = Utilisateur::paginate(15);
        return response()
        ->json($utilisateurs);
    }
    
    public function getUtilisateursByName($name)
    {
        $utilisateurs = Utilisateur::where('nom', 'like', "%${name}%")->paginate(15);
        return response()
        ->json($utilisateurs)
        ->header('Access-Control-Allow-Origin', '*');
    }
    
    public function getReferants(Request $request)
    {
        $referants = Utilisateur::where('type', '<=', 2)->get();
        return response()
        ->json($referants);
    }
    
    public function getUtilisateur($id)
    {
        $utilisateur = Utilisateur::find($id);
        if (!$utilisateur) {
            return (new Response(null, 404));
        }
        return response()->json($utilisateur);
    }
    
    private function loginExist ($login)
    {
        $utilisateur = Utilisateur::where('login', '=', $login)->first();
        return isset($utilisateur);
    }
    
    public function postUtilisateur(Request $request)
    {
        $data = json_decode($request->getContent());
        
        if ($this->loginExist($data->login)) {
            return response()->json(['status' => 'loginExist']);
        }
        
        $utilisateur = new Utilisateur();
        $utilisateur->nom = $data->nom;
        $utilisateur->login = $data->login;
        $utilisateur->type = $data->type;
        $utilisateur->password = app('hash')->make('pac123');
        $utilisateur->save();
        return response()->json(['status' => 'success', 'id' => $utilisateur->id]);
    }
    
    public function putUtilisateur($id, Request $request)
    {
        $data = json_decode($request->getContent());
        $utilisateur = Utilisateur::find($id);
        
        if ($utilisateur->login !== $data->login) {
            if ($this->loginExist($data->login)) {
                return response()->json(['status' => 'loginExist']);
            }
            $utilisateur->login = $data->login;
        }
        if ($utilisateur->nom !== $data->nom) {
            $utilisateur->nom = $data->nom;
        }
        if ($utilisateur->type !== $data->type) {
            $utilisateur->type = $data->type;
        }
        $utilisateur->save();
        
        /* recupere le tout */
        $utilisateur = Utilisateur::find($id);
        return response()->json(['status' => 'success', 'data' => $utilisateur]);
    }
    
    public function delUtilisateur($id)
    {
        $utilisateur = Utilisateur::find($id);
        $utilisateur->delete();
        return response()->json(['status' => 'success']);
    }
}