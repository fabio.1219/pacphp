<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('categorie', function (Blueprint $table) {
            $table->increments('id');
            $table->string('connaissance');
            $table->string('potentiel');
            $table->timestamps();
        });
        Schema::create('utilisateur', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->char('password', 255);
            $table->string('login');
            $table->integer('type');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('groupe');
            $table->string('secteur');
            $table->integer('referant')->nullable()
            ->foreign()->references('id')->on('utilisateur');
            $table->integer('categorie')->nullable()
            ->foreign()->references('id')->on('categorie');
            $table->boolean('deleted')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('contactclient', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client')
            ->foreign()->references('id')->on('client');
            $table->string('nom');
            $table->string('telephone')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
        Schema::create('typeaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('typeactionCategorie', function (Blueprint $table) {
            $table->integer('typeaction')
            ->foreign()->references('id')->on('typeaction');
            $table->integer('categorie')
            ->foreign()->references('id')->on('categorie');
            $table->timestamps();
        });
        Schema::create('action', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client')
            ->foreign()->references('id')->on('client');
            $table->integer('typeaction')
            ->foreign()->references('id')->on('typeaction');
            $table->string('detail')->nullable();
            $table->string('dateEcheance')->nullable();
            $table->string('datePrevue')->nullable();
            $table->string('dateEcheanceCompare')->nullable();
            $table->string('datePrevueCompare')->nullable();
            $table->timestamps();
        });
        Schema::create('etat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->timestamps();
        });
        Schema::create('etatAction', function (Blueprint $table) {
            $table->integer('utilisateur')
            ->foreign()->references('id')->on('utilisateur');
            $table->integer('action')
            ->foreign()->references('id')->on('action');
            $table->integer('etat')
            ->foreign()->references('id')->on('etat');
            $table->string('commentaire')->nullable();
            $table->timestamps();
        });
        Schema::create('statistiques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nbClients');
            $table->integer('sansCategories');
            $table->text('groupes');
            $table->timestamps();
        });
        Schema::create('compterendu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client')
            ->foreign()->references('id')->on('client');
            $table->integer('utilisateur')
            ->foreign()->references('id')->on('utilisateur');
            $table->string('texte')->nullable();
            $table->timestamps();
        });
        Schema::create('token', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('utilisateur')
            ->foreign()->references('id')->on('utilisateur');
            $table->string('token', 511);
            $table->timestamps();
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::drop('etatAction');
        Schema::drop('etat');
        Schema::drop('action');
        Schema::drop('typeactionCategorie');
        Schema::drop('typeaction');
        Schema::drop('contactclient');
        Schema::drop('client');
        Schema::drop('utilisateur');
        Schema::drop('categorie');
        Schema::drop('statistiques');
        Schema::drop('compterendu');
        Schema::drop('token');
    }
}