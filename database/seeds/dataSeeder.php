<?php

use Illuminate\Database\Seeder;

class dataSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table('utilisateur')->insert([
        'id' => 1,
        'login' => 'admin',
        'nom' => 'admin',
        'password' => app('hash')->make('admin'),
        'type' => 0
        ]);
        
        
        DB::table('client')->insert([
        'id' => 1,
        'nom' => 'Migros',
        'secteur' => 'Genève',
        'groupe' => 'Entreprise',
        'categorie' => 9,
        'referant' => 1
        ]);
        DB::table('client')->insert([
        'id' => 2,
        'nom' => 'Balexert',
        'secteur' => 'Genève',
        'groupe' => 'Entreprise',
        'categorie' => 6,
        'referant' => 1
        ]);
        DB::table('client')->insert([
        'id' => 3,
        'nom' => 'Planète Charmilles',
        'secteur' => 'Genève',
        'groupe' => 'Entreprise',
        'categorie' => 5,
        'referant' => 1
        ]);
        DB::table('client')->insert([
        'nom' => 'Naef',
        'secteur' => 'Genève',
        'groupe' => 'Régie',
        'categorie' => 4,
        'referant' => 1
        ]);
        DB::table('client')->insert([
        'nom' => 'FIFA',
        'secteur' => 'Genève',
        'groupe' => 'Association',
        'categorie' => 7,
        'referant' => 1
        ]);
        DB::table('client')->insert([
        'nom' => 'Etat de Genève',
        'secteur' => 'Genève',
        'groupe' => 'Entreprise public',
        'categorie' => 5,
        'referant' => 1
        ]);
        DB::table('client')->insert([
        'nom' => 'Commune de Vernier',
        'secteur' => 'Genève',
        'groupe' => 'Entreprise public',
        'categorie' => 3,
        'referant' => 1
        ]);
    }
}