<?php

use Illuminate\Database\Seeder;

class populateSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table('categorie')->insert([
        'id' => 1,
        'connaissance' => 'basse',
        'potentiel' => 'faible'
        ]);
        DB::table('categorie')->insert([
        'id' => 2,
        'connaissance' => 'basse',
        'potentiel' => 'moyen'
        ]);
        DB::table('categorie')->insert([
        'id' => 3,
        'connaissance' => 'basse',
        'potentiel' => 'fort'
        ]);
        DB::table('categorie')->insert([
        'id' => 4,
        'connaissance' => 'moyenne',
        'potentiel' => 'faible'
        ]);
        DB::table('categorie')->insert([
        'id' => 5,
        'connaissance' => 'moyenne',
        'potentiel' => 'moyen'
        ]);
        DB::table('categorie')->insert([
        'id' => 6,
        'connaissance' => 'moyenne',
        'potentiel' => 'fort'
        ]);
        
        DB::table('categorie')->insert([
        'id' => 7,
        'connaissance' => 'élevée',
        'potentiel' => 'faible'
        ]);
        DB::table('categorie')->insert([
        'id' => 8,
        'connaissance' => 'élevée',
        'potentiel' => 'moyen'
        ]);
        DB::table('categorie')->insert([
        'id' => 9,
        'connaissance' => 'élevée',
        'potentiel' => 'fort'
        ]);
        DB::table('etat')->insert([
        'id' => 1,
        'nom' => 'A valider'
        ]);
        DB::table('etat')->insert([
        'id' => 2,
        'nom' => 'Envisagée'
        ]);
        DB::table('etat')->insert([
        'id' => 3,
        'nom' => 'Prévue'
        ]);
        DB::table('etat')->insert([
        'id' => 4,
        'nom' => 'Réalisée'
        ]);
        DB::table('etat')->insert([
        'id' => 5,
        'nom' => 'Annulée'
        ]);
    }
}