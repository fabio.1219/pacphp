<?php
use \Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'api'], function () use ($app) {
    $app->post('/login[/]', 'LoginController@login');
    $app->group(['middleware' => 'auth'], function () use ($app) {
        // changer mdp
        $app->post('/login/password[/]', 'LoginController@changerMdp');
        $app->get('/logout[/]', 'LoginController@logout');
        //route /actions
        
        $app->get('/actions[/]', 'ActionController@getActions');
        $app->get('/actions/{id}[/]', 'ActionController@getAction');
        $app->post('/actions[/]', 'ActionController@postAction');
        $app->put('/actions/{id}[/]', 'ActionController@putAction');
        
        //route /actionsdefinitives
        
        $app->get('/actionsdefinitives[/]', 'ActionDefinitiveController@getActionsDefinitives');
        $app->get('/actionsdefinitives/{id}[/]', 'ActionDefinitiveController@getActionDefinitive');
        
        // route /categories
        $app->get('/categories/{idCategorie}/{idReferant}[/]', 'CategorieController@getCategorie');
        
        // route /comptesrendus
        $app->post('/comptesrendus[/]', 'CompterenduController@postCompterendu');
        
        // route /descriptioncategories
        
        $app->get('/descriptioncategories[/]', 'DescriptionCategorieController@getCategories');
        $app->get('/descriptioncategories/{id}[/]', 'DescriptionCategorieController@getCategorie');
        
        // route /clients
        
        $app->get('/clients[/]', 'ClientController@getClients');
        $app->get('/clients/{id}[/]', 'ClientController@getClient');
        $app->post('/clients[/]', 'ClientController@postClient');
        $app->put('/clients/{id}[/]', 'ClientController@putClient');
        $app->delete('/clients/{id}[/]', 'ClientController@delClient');
        
        // route /referants
        $app->get('/referants[/]', 'UtilisateurController@getReferants');
        
        // route /clients
        
        $app->get('/contactsclients[/]', 'ContactClientController@getContactsClients');
        $app->get('/contactsclients/{id}[/]', 'ContactClientController@getContactClient');
        
        // route /etats
        
        $app->get('/etats[/]', 'EtatController@getEtats');
        
        //route /typesaction
        
        $app->get('/typesaction[/]', 'TypeActionController@getTypesAction');
        $app->get('/typesaction/{id}[/]', 'TypeActionController@getTypeAction');
        $app->post('/typesaction[/]', 'TypeActionController@postTypeAction');
        $app->put('/typesaction/{id}[/]', 'TypeActionController@putTypeAction');
        $app->delete('/typesaction/{id}[/]', 'TypeActionController@delTypeAction');
        
        // route /utilisateurs
        
        $app->get('/utilisateurs[/]', 'UtilisateurController@getUtilisateurs');
        $app->get('/utilisateurs/{id}[/]', 'UtilisateurController@getUtilisateur');
        $app->post('/utilisateurs[/]', 'UtilisateurController@postUtilisateur');
        $app->put('/utilisateurs/{id}[/]', 'UtilisateurController@putUtilisateur');
        $app->delete('/utilisateurs/{id}[/]', 'UtilisateurController@delUtilisateur');
        
        // route /statistiques
        $app->get('statistiques/clients[/]', 'StatistiqueController@getStatistiquesClients');
        $app->get('statistiques/saved[/]', 'StatistiqueController@getStatistiquesEnregistrees');
        $app->post('statistiques/saved[/]', 'StatistiqueController@postStatistiquesEnregistrees');
        $app->delete('statistiques/saved/{id}[/]', 'StatistiqueController@delStatistiquesEnregistrees');
    });
});

$app->get('/[{route}]', function() use ($app) {
    return view('index');
});